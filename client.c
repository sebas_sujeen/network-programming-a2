#include "client.h"

/* In our case, we have only one producer and a consumer, so strictly speaking there
 * is no need for 2 cond vars, but still we do it because then it can be extended to
 * multiple consumers */
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t p_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t c_cond = PTHREAD_COND_INITIALIZER;

static struct scirc_buffer *circ = NULL;
static int pipefds[2];
static int lse = 0;
static float prob = 0.0;
static void alarm_handler(int sig)
{
	Write(pipefds[1], "", 1);
	return;
}

/* this is the initial client send and recv before recving the actual file */
static void initcli_send_and_recv_with_timeout(int sockfd, void *data, size_t size, struct sockaddr_in *serv_addr)
{

	struct itimerval it;

	/* snd buf's last data gram sent, last ACK received and rbuf's last datagram received, avoids,
	 * note that the ACK in this case is not explicit, the ACK is send as a dgram that contains the ephemeral port */
	it.it_interval.tv_sec = 0;
	it.it_interval.tv_usec = 200000;
	it.it_value.tv_sec = 0;
	it.it_value.tv_usec = 200000;
	setitimer(ITIMER_REAL, &it, NULL);
	
	fd_set rset;
	int state = 0;

	for(;;) {
		/* write the filename out */	
		if ( state == 0 ) {
			printf("<client> sending FILE: %s to the server\n", (char *)data);
			Write(sockfd, data, size);
		}
		FD_ZERO(&rset);
		FD_SET(sockfd, &rset);
		FD_SET(pipefds[1], &rset);

		int ret, maxfd;
		/* now wait for ephemeral port no or timeout*/
		maxfd = max(pipefds[0], sockfd) + 1;
again:
		if ( (ret = select(maxfd, &rset, NULL, NULL, NULL)) < 0 ) {
			if (errno == EINTR) 
				goto again;
			else 
				err_sys("select error");
		}
		/* getting the ephemeral port back is similar to getting an ACK */
		if (FD_ISSET(sockfd, &rset)) {
			char port_buf[sizeof("65535") + 1];
			int new_port ;

			Read(sockfd, port_buf, sizeof(port_buf));
			printf("<client> New ephermeral port = %s\n", port_buf);
			new_port = atoi(port_buf);
			/* At this point we need to "reconnect" the socket to this port */
			serv_addr->sin_port = htons(new_port);
			Connect(sockfd, (void *) serv_addr, sizeof(*serv_addr));
			/* state = 1 implies reconnect is successful..We need these states to distinguish between
			 * filename loss and ACK loss */
			state = 1;
			printf("<client> Sending ACK to the server\n");
			Write(sockfd, "ACK", 3);
			break;
		}
		if (FD_ISSET(pipefds[0], &rset)) {
			char c;
			printf("<client> Timeout in sending FILE: %s to the server\n", (char *)data);
			/* read the byte off and discard */
			Read(pipefds[0], &c, 1);
			continue;
		}
	}
	/* clear out the timer */
	memset(&it, 0x0, sizeof(it));
	setitimer(ITIMER_REAL, &it, NULL);
}

static void send_dg(int sockfd, struct prot_header *h)
{

	struct dgram d;
	
	memcpy(&d.p, h, sizeof(*h));
	Write_with_prob(sockfd, &d, sizeof(d), prob);

}

static void *consumer(void *arg)
{
	struct dgram *d;
	struct cons_info *cons = arg;
	struct prot_header *h;

	int prev_free, sockfd = cons->sockfd;
	int shld_exit = 0;
	char *p;
	char buf[DGRAM_SZ - sizeof(struct prot_header) + 1];
	int mu = cons->mu;
	float time_to_sleep;
	int usec;
	struct timeval tv;

	for (;;) {
		time_to_sleep = -1 * log(get_rand()) * mu;
		usec = (int) time_to_sleep * 1000;
		tv.tv_sec = 0;
		tv.tv_usec = usec;
		select(0, NULL, NULL, NULL, &tv);
		while (circ->filled_entries > 0) {
			prev_free = circ->elem_count - circ->filled_entries;
			d = remove_from_scirc_buffer(circ, m);
			h = &d->p;
			if (prev_free == 0) {
				/* send a window update packet */
				h->ack_no = lse;
				h->eof_marker = 2;
				h->rsw_sz = circ->elem_count - circ->filled_entries;
				printf("<client> Sending a window update with current window size : %d\n", h->rsw_sz);
				send_dg(sockfd, h);
			}
			if (h->eof_marker == 1)
				shld_exit = 1;
			else {
				p = (char *)d + sizeof(struct prot_header);
				memcpy(buf, p, sizeof(buf) -1);
				buf[sizeof(buf) -1] = '\0';
				printf("%s\n", buf);
			}
		}
		if (shld_exit) {
			printf("<client> Consumer thread finished printing..Exiting\n");
			return NULL;
		}
	}
}


static void producer(int sockfd, struct dgram *dg)
{
	struct prot_header *h = &dg->p;
	int len, i;
	
	if (h->eof_marker == 2) {
		/* window probe packet */
		h->rsw_sz = circ->elem_count - circ->filled_entries;
		send_dg(sockfd, h);
		return;
	}
	if (h->seq_no == lse) {
		printf("<client> Received an datagram with seq_no : %d, expected seq_no = %d, implies an inorder datagram\n", h->seq_no, lse);
		len = circ->elem_count - circ->filled_entries;
		if (len == 0) {
			printf("<client> Cannot receive datagram, receiver window size: 0\n");
			h->rsw_sz = 0;
			h->ack_no = lse;
			send_dg(sockfd, h);
			return;
		}

		/* space available, write data to circ_buffer */
		add_to_scirc_buffer(circ, dg, sizeof(*dg), m);
		lse ++;
		i = 0;
		while (circ->valid[circ->prod_idx] == 1 && i < (circ->elem_count - circ->filled_entries)) {
			circ->prod_idx = (circ->prod_idx + 1) % circ->elem_count;
			lse ++;
			i++;
		}
	
		/* send ACK back */
		h->ack_no = lse;
		h->rsw_sz = circ->elem_count - circ->filled_entries;
		send_dg(sockfd, h);
	}
	else if (h->seq_no < lse) {
		printf("<client> Received a datagram with seq_no : %d and expected seq_no : %d, implies a duplicate datagram\n", h->seq_no, lse);
		/* this means we got all the data till LSE */
		h->ack_no = lse;
		h->rsw_sz = circ->elem_count - circ->filled_entries;
		send_dg(sockfd, h);
	}
	else {
		/* out of order seq_no, need to send duplicate ACK */
		int idx;
		printf("<client> Received a datagram with seq_no : %d and expected seq_no: %d, implies an out of order datagram, sending a dup ack\n", h->seq_no, lse);	
		idx = h->seq_no % circ->elem_count;
		if (circ->valid[idx]) {
			h->ack_no = lse;
			h->rsw_sz = circ->elem_count - circ->filled_entries;
			send_dg(sockfd, h);
			return;
		}
		else {
			add_to_scirc_buffer_index(circ, dg, sizeof(*dg), idx, m);
			h->ack_no = lse;
			h->rsw_sz = circ->elem_count - circ->filled_entries;
			send_dg(sockfd, h);
		}
	}

}


static void recv_datagrams_client(int sockfd)
{
	struct dgram dg;
	struct prot_header *h;

	for (;;) {
		Read(sockfd, &dg, sizeof(dg));
		producer(sockfd, &dg);
		h = &dg.p;
		if (h->eof_marker == 1)
			break;
	}
}
/* error checking for values is avoided deliberately assuming that inputs in
 * client.in are not stupid */
static void populate_cli_info(FILE *fp, struct cli_info *pcli)
{
	char buf[MAXLINE];
	char *pos;

	/* populate information one at a time */
	Fgets(buf, sizeof(buf), fp);
	/* fgets() includes the newline in the buffer, strip it */
	if ( (pos = strchr(buf, '\n')) != NULL)
		*pos = '\0';
	Inet_pton(AF_INET, buf, &(pcli->serv_addr));
	Fgets(buf, sizeof(buf), fp);
	if ( (pos = strchr(buf, '\n')) != NULL)
		*pos = '\0';
	/* atoi doesn't return error, assume port number is proper */
	pcli->port = atoi(buf);
	Fgets(pcli->filename, sizeof(pcli->filename), fp);
	if ( (pos = strchr(pcli->filename, '\n')) != NULL)
		*pos = '\0';
	Fgets(buf, sizeof(buf), fp);
	if ( (pos = strchr(buf, '\n')) != NULL)
		*pos = '\0';
	pcli->recv_swsz = atoi(buf);
	Fgets(buf, sizeof(buf), fp);
	if ( (pos = strchr(buf, '\n')) != NULL)
		*pos = '\0';
	pcli->seed = atoi(buf);
	srand(time(NULL));
	Fgets(buf, sizeof(buf), fp);
	if ( (pos = strchr(buf, '\n')) != NULL)
		*pos = '\0';
	pcli->prob = 1 - atof(buf);
	prob = pcli->prob;
	Fgets(buf, sizeof(buf), fp);
	if ( (pos = strchr(buf, '\n')) != NULL)
		*pos = '\0';
	pcli->mu = atoi(buf);

	printf("<client> filename = %s, port = %d, recv_swsz = %d, seed = %d, prob = %f, mu = %d\n", pcli->filename, pcli->port, pcli->recv_swsz, 
			pcli->seed, pcli->prob, pcli->mu);

	fclose(fp);
}

int main()
{
	struct cli_info *pcli;
	struct common_ifi_info **s;
	struct common_addr_info *c;
	int count = 0;
	struct sockaddr_in cli_addr, serv_addr;
	int sockfd;
	pthread_t tid;
	Signal(SIGALRM, alarm_handler);
	Pipe(pipefds);
	FILE *fp = Fopen("client.in", "r");

	pcli = Malloc(sizeof(*pcli));
	populate_cli_info(fp, pcli);
	circ = init_scirc_buffer(DGRAM_SZ, pcli->recv_swsz);
	count = get_ifi_count();
	s = populate_ifi_info(count); 
	c = compute_longest_prefix(s, &pcli->serv_addr, count);

	char buf[INET_ADDRSTRLEN];
	char buf1[INET_ADDRSTRLEN];
	Inet_ntop(AF_INET, &c->dst_addr, buf, sizeof(buf));
	Inet_ntop(AF_INET, &c->src_addr, buf1,  sizeof(buf));
	switch(c->route_flag) {
		case SAMEHOST:
			printf("<client> %s and %s belongs to same host\n", buf, buf1);
			break;
		case SAMESUBNET:
			printf("<client> %s and %s belongs to the same subnet\n", buf, buf1);
			break;
		default:
			printf("<client> %s and %s belongs to different subnet\n", buf, buf1);

	}
	printf("<client> IPClient: %s, IPServer: %s\n", buf1, buf);
	memset(&cli_addr, 0x0, sizeof(cli_addr));
	memset(&serv_addr, 0x0, sizeof(serv_addr));

	cli_addr.sin_family = serv_addr.sin_family = AF_INET;
	/* let the kernel decide the port no */
	cli_addr.sin_port = htons(0);
	serv_addr.sin_port = htons(pcli->port);
	cli_addr.sin_addr.s_addr = c->src_addr.s_addr;
	serv_addr.sin_addr.s_addr = c->dst_addr.s_addr;

	sockfd = Socket(AF_INET, SOCK_DGRAM, 0);
	Bind(sockfd, (void *)&cli_addr, sizeof(cli_addr));
	int enable = 1;
	Setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
	struct sockaddr_in tmp;
	socklen_t len = sizeof(tmp);
	memset(&tmp, 0x0, sizeof(tmp));
	Getsockname(sockfd, (void *)&tmp, &len);
	printf("<client> Sock name is %s and port is %d\n", Sock_ntop_host((void *)&tmp, len), ntohs(tmp.sin_port));

	Connect(sockfd, (void *)&serv_addr, sizeof(serv_addr));
	len = sizeof(tmp);
	memset(&tmp, 0x0, sizeof(tmp));
	Getpeername(sockfd, (void *)&tmp, &len);
	printf("<client> Peer name is %s and port is %d\n", Sock_ntop_host((void *)&tmp, len), ntohs(tmp.sin_port));

	initcli_send_and_recv_with_timeout(sockfd, pcli->filename, strlen(pcli->filename) + 1, &serv_addr);
	struct cons_info *cons = Malloc(sizeof(*cons));
	
	cons->sockfd = sockfd;
	cons->mu = pcli->mu;

	Pthread_create(&tid, NULL, consumer, cons);	
	recv_datagrams_client(sockfd);
	/* wait for the consumer thread to finish printing */
	Pthread_join(tid, NULL);
	return 0;
}
