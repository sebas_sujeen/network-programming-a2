#include "server.h"
#define MAX_TIMEOUT_TRIES 50
struct circ_buffer *circ = NULL;
static int pipefds[2];
static int timeout_cnt = 0;
static int ca_ack = 0;
static void alarm_handler(int sig)
{
	Write(pipefds[1], "", 1);
	return;
}
void sigchld_handler(int sig)
{
	pid_t child_pid;
	int status;
	while((child_pid = waitpid(-1, &status, WNOHANG )) > 0) 
		; // dont print because printf() is not async-signal-safe
}

static void bind_ifi(struct common_ifi_info **s, int count, int port)
{
	int sockfd, i;
	struct sockaddr_in bsa; 

	for (i = 0 ; i < count ; i++) {
		sockfd = Socket(AF_INET, SOCK_DGRAM, 0);
		/* The other alternative is to typecast the struct sockaddr to struct sockaddr_in, which
		 * IMO is more ugly looking..so much for portability */
		memcpy(&bsa, &s[i]->ip_addr, sizeof(bsa));
		bsa.sin_port = htons(port);
		Bind(sockfd, (void *)&bsa, sizeof(bsa));
		int enable = 1;
		Setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int));
		s[i]->sockfd = sockfd;
	}
}

static int find_max_fd(struct common_ifi_info **s, int count)
{
	int max_fd = -1, i ;

	for (i = 0; i < count ; i ++) {
		if (s[i]->sockfd > max_fd )
			max_fd = s[i]->sockfd;
	}
	return max_fd;
}

static void populate_serv_info(FILE *fp, struct serv_info *s)
{
	char buf[MAXLINE];
	char *pos;

	Fgets(buf, sizeof(buf), fp);
	if ( (pos = strchr(buf, '\n')) != NULL)
		*pos = '\0';
	s->port = atoi(buf);
	Fgets(buf, sizeof(buf), fp);
	if ( (pos = strchr(buf, '\n')) != NULL)
		*pos = '\0';
	s->ssw_sz = atoi(buf);
	fclose(fp);
}

static inline struct dgram *get_dgram(struct scirc_buffer *s, int index)
{
	struct dgram *d = s->data;
	return (void *)&d[index];

}

static void send_dg_iovec(int sockfd, struct scirc_buffer *s, int index, struct state_info *st)
{
	struct msghdr m;
	struct iovec iov[2];

	memset(&m, 0x0, sizeof(m));
	struct dgram *dg = get_dgram(s, index);
	dg->p.timestamp = rtt_ts(&rttinfo);

	/* since the socket is already connected, the msg_name can be NULL */
	m.msg_name = NULL;
	m.msg_iovlen = 2;
	iov[0].iov_base = (void *)&dg->p;
	iov[0].iov_len = sizeof(dg->p);
	iov[1].iov_base = (void *)dg->data;
	/* includes the NULL byte always, the spec says the file is ASCII text, so NULL
	 * byte can be safely used as a terminator and the data can be treated as a string */
	iov[1].iov_len = DGRAM_SZ - sizeof(dg->p);
	m.msg_iov = iov;

	/* m.msg_flags is ignored by sendmsg(), so set the DONT_ROUTE appropriately in the 
	 * flags argument of sendmsg() */
	Sendmsg(sockfd, &m, st->flag);
}

static void print_details(char *str, struct state_info *st, int dupack_cnt)
{
	printf("%s", str);
	char phase[100];
	if (st->phase == SLOW_START) 
		strcpy(phase, "SLOW START");
	else if (st->phase == CA)
		strcpy(phase, "CONGESTION AVOIDANCE");
	else 
		strcpy(phase, "FAST RETRANSMIT AND RECOVERY");
	printf("\t<server> Duplicate ACK count = %d, phase = %s, cwnd = %d, rwnd = %d  ssthresh = %d\n", dupack_cnt, phase, (int)st->cwnd, st->p_hdr.rsw_sz, st->ssthresh);
}

void send_dgrams_with_timeout( int sockfd, struct scirc_buffer *s, struct state_info *st)
{

	struct itimerval it;
	int dupack_cnt = 0;
	it.it_interval.tv_sec = 0;
	it.it_interval.tv_usec = 200000;
	it.it_value.tv_sec = 0;
	it.it_value.tv_usec = 200000;

	int i, index;
	rtt_newpack(&rttinfo);
	setitimer(ITIMER_REAL, &it, NULL);


	for (i = 0, index = s->lar ; i < (int)st->cwnd; i++, index = (index + 1) % s->elem_count) {
		send_dg_iovec(sockfd, s, index, st);
	}	

	fd_set rset;
	struct dgram rcv_dgram, *chk_dgram;
	struct prot_header *rcv_hdr, *chk_hdr;
	int n = (int)st->cwnd;

	for(i = 0; i < n; i++) {
		FD_ZERO(&rset);
		FD_SET(sockfd, &rset);
		FD_SET(pipefds[0], &rset);

		int ret, maxfd;
		maxfd = max(pipefds[0], sockfd) + 1;
again:
		if ( (ret = select(maxfd, &rset, NULL, NULL, NULL)) < 0 ) {
			if (errno == EINTR) 
				goto again;
			else 
				err_sys("select error");
		}
		if (FD_ISSET(sockfd, &rset)) {
			/* do main stuff here */
			Read_mod(sockfd, &rcv_dgram, sizeof(rcv_dgram));
			rcv_hdr = &rcv_dgram.p;

			if (rcv_hdr->eof_marker == 2) {
				printf("<server> Got a window update ACK from the client with flow control window size = %d\n", rcv_hdr->rsw_sz);
				if (dupack_cnt >= 3) {
					send_dg_iovec(sockfd, s, s->lar, st);
					dupack_cnt = 0;
				}
				i--;
				st->p_hdr.rsw_sz = rcv_hdr->rsw_sz;
				continue;
			}
			/* update the flow control window */
			st->p_hdr.rsw_sz = rcv_hdr->rsw_sz;
			chk_dgram = get_dgram(s, s->lar);
			chk_hdr = &chk_dgram->p;
			if ((rcv_hdr->ack_no ) >= (chk_hdr->seq_no + 1)) {
				timeout_cnt = 0;
				/* got an inorder CACK */
				int k, cnt, n_ack = 0;
				cnt = rcv_hdr->ack_no - chk_hdr->seq_no;
				for (k = 0; k < cnt; k++) {
					(void)remove_from_scirc_buffer(s);
					n_ack ++;
				}
				switch(st->phase) {
					case SLOW_START:
						st->cwnd = st->cwnd + (double) n_ack;
						dupack_cnt = 0;
						if ((int)st->cwnd >= st->ssthresh) {
							st->phase = CA;
						}
						break;
					case CA:
						dupack_cnt = 0;
						ca_ack ++;
						if (ca_ack >= (int)st->cwnd) {
							ca_ack = 0;
							st->cwnd += 1;
						}
						break;
					case FRR:
						st->cwnd = (int)st->ssthresh;
						dupack_cnt = 0;
						st->phase = CA;
						break;
				}
				print_details("<server> Got an inorder ACK, which results in :\n", st, dupack_cnt);
			}
			else {
				/* duplicate ACK */
				timeout_cnt = 0;
				switch(st->phase) {
					case SLOW_START:
					case CA:
						dupack_cnt ++;
						if (dupack_cnt >= 3 && st->p_hdr.rsw_sz > 0) {
							/* send the dgram here */
							dupack_cnt = 0;
							send_dg_iovec(sockfd, s, s->lar, st);
							st->ssthresh = max ((int)st->cwnd / 2, 1);
							st->cwnd = (double)st->ssthresh;
							st->phase = FRR;
						}
						break;
					case FRR:
						st->cwnd += 1;	
						break;
				}
				print_details("<server> Got an out of order ACK, which results in :\n", st, dupack_cnt);
			}

			/* regardless  of whether we get an inorder ACK or not, we check the window size to see whether we need to
			 * go to the persistent timer state */
		}
		if (FD_ISSET(pipefds[0], &rset)) {
			char c;
			timeout_cnt ++;
			if (timeout_cnt == MAX_TIMEOUT_TRIES) {
				printf("<server> Got %d timeouts consequently..terminating connection with the client\n", timeout_cnt);
				exit(0);
			}
			/* read the byte off and discard */
			Read_mod(pipefds[0], &c, 1);
			switch(st->phase) {
				case SLOW_START:
					st->ssthresh = max ((int)st->cwnd / 2, 1);
					st->cwnd = (int)1;
					dupack_cnt = 0;
					break;
				case CA:
				case FRR:
					st->ssthresh = max((int)st->cwnd / 2, 1);
					st->cwnd = 1.0;
					dupack_cnt = 0;
					st->phase = SLOW_START;
					break;
			}
			print_details("<server> Got a timeout, which results in :\n", st, dupack_cnt);
			printf("<server> Current timeout count = %d\n", timeout_cnt);
		}
	}

	/* clear out the timer */
	memset(&it, 0x0, sizeof(it));
	setitimer(ITIMER_REAL, &it, NULL);
}

static void send_window_probe(int sockfd, int flag)
{
	char buf[DGRAM_SZ - sizeof(struct prot_header)];
	struct prot_header h;
	struct msghdr m;
	struct iovec iov[2];
	
	memset(&h, 0x0, sizeof(h));
	h.eof_marker = 2;
	

	memset(&m, 0x0, sizeof(m));

	/* since the socket is already connected, the msg_name can be NULL */
	m.msg_name = NULL;
	m.msg_iovlen = 2;
	iov[0].iov_base = (void *) &h;
	iov[0].iov_len = sizeof(h);
	iov[1].iov_base = (void *)buf;
	iov[1].iov_len = sizeof(buf);
	m.msg_iov = iov;

	/* m.msg_flags is ignored by sendmsg(), so set the DONT_ROUTE appropriately in the 
	 * flags argument of sendmsg() */
	Sendmsg(sockfd, &m, flag);
}

static void start_persistent_timer(int sockfd, struct state_info *st)
{

	int ret, maxfd;
	fd_set rset;
	struct itimerval it;
	struct dgram w_dgram;
	struct prot_header *w_hdr;
	
	it.it_interval.tv_sec = 0;
	it.it_interval.tv_usec = 200000;
	it.it_value.tv_sec = 0;
	it.it_value.tv_usec = 200000;
	memset(&w_dgram, 0x0, sizeof(w_dgram));

	setitimer(ITIMER_REAL, &it, NULL);
	send_window_probe(sockfd, st->flag);

	for (;;) {
		FD_ZERO(&rset);
		FD_SET(pipefds[0], &rset);
		FD_SET(sockfd, &rset);
		maxfd = max(sockfd, pipefds[0]) + 1;
again:
		if ( (ret = select(maxfd, &rset, NULL, NULL, NULL)) < 0) {
			if (errno == EINTR)
				goto again;
			else
				err_sys(" select error");
		}
		if (FD_ISSET(sockfd, &rset)) {
			Read_mod(sockfd, &w_dgram, sizeof(w_dgram));
			w_hdr = (void *)&w_dgram;	
			if (w_hdr->eof_marker == 2) {
				printf("<server> Got a window update ack from the client with flow control window size = %d\n", w_hdr->rsw_sz);
				st->p_hdr.rsw_sz = w_hdr->rsw_sz;
				st->cwnd = (float) w_hdr->rsw_sz;
				if (w_hdr->rsw_sz > 0)
					break;
			}
		}
		if (FD_ISSET(pipefds[0], &rset)) {
			char c;
			Read_mod(pipefds[0], &c, 1);
			printf("<server> In persistent timer mode, got timeout..trying again\n");
			send_window_probe(sockfd, st->flag);
		}

	}
	memset(&it, 0x0, sizeof(it));
	setitimer(ITIMER_REAL, &it, NULL);
}

static void handle_file_transfer(int sockfd, char *filename, int flag, struct serv_info *info)
{
	FILE *fp = Fopen(filename, "r");

	struct dgram *dgram;
	static struct state_info st;
	int  n_read;;
	char buf[DGRAM_SZ - sizeof(struct prot_header)];
	int n_dgrams, i, shld_exit = 0;

	Signal(SIGALRM, alarm_handler);

	dgram = Malloc(sizeof(*dgram));
	init_prot_header(&st, flag);
	struct scirc_buffer *sbuf = init_scirc_buffer(DGRAM_SZ, info->ssw_sz);

	for (;;) {
		n_dgrams = min((int)st.cwnd, st.p_hdr.rsw_sz);
		n_dgrams = min(n_dgrams, info->ssw_sz);
		st.cwnd = (double)n_dgrams;
		printf("<server> min( congestion control window, flow control window, max sender window) : %d\n", (int)st.cwnd);
		check_for_space(sbuf, n_dgrams);
		if (n_dgrams == 0) {
			printf("<server> Client's flow control window is 0, switching to persistent timer mode\n");
			start_persistent_timer(sockfd, &st);
			printf("<server> Getting out of  persistent timer mode with flow control window size =  %d\n", st.p_hdr.rsw_sz);
			n_dgrams = min((int)st.cwnd, st.p_hdr.rsw_sz);
			n_dgrams = min(n_dgrams, sbuf->elem_count - sbuf->filled_entries);
			st.cwnd = (double)n_dgrams;
		}
		i = 0;
		sbuf->lds = sbuf->lar;
		for (i = 0; i < n_dgrams; i++) {
			if (sbuf->valid[sbuf->lds]) {
				sbuf->lds = (sbuf->lds + 1) % sbuf->elem_count;
				continue;
			}
			n_read = Read_mod(fileno(fp), buf, sizeof(buf));
			if (n_read == 0)
				break;
			construct_dgram(dgram, buf, sizeof(buf), &st);
			add_to_scirc_buffer(sbuf, dgram, sizeof(dgram));
		}
		/* at this point there could be a case where i < n_grams ie EOF reached before 
		 * the cwnd could be filled.. */
		if (i < n_dgrams) {
			/* eof occured, so adjust cwnd and set eof_marker in pkh hdr accordingly */
			construct_dgram(dgram, buf, sizeof(buf), &st);
			dgram->p.eof_marker = 1;
			add_to_scirc_buffer(sbuf, dgram, sizeof(dgram));
			shld_exit = 1;
		}
		send_dgrams_with_timeout(sockfd, sbuf, &st);
		assert(sbuf->filled_entries >= 0);
		if (shld_exit == 1)
			break;
	}

}

int main()
{
	FILE *fp = Fopen("server.in", "r");
	struct serv_info *sinfo;
	struct common_ifi_info **s;
	int i = 0,  max_fd, count = 0;
	char filename[PATH_MAX];
	pid_t pid;
	int state = 0;
	Pipe(pipefds);
	Signal(SIGCHLD, sigchld_handler);
	Signal(SIGALRM, SIG_IGN);
	sinfo = Malloc(sizeof(*sinfo));
	memset(sinfo, 0x0, sizeof(*sinfo));
	populate_serv_info(fp, sinfo);
	sinfo->rsw_sz = INT_MAX;

	count = get_ifi_count();
	s = populate_ifi_info(count); 
	bind_ifi(s, count, sinfo->port);	
	max_fd = find_max_fd(s, count) + 1;

	fd_set rset;

	for (;;) {
		FD_ZERO(&rset);
		for (i = 0; i < count ; i++) {
			FD_SET(s[i]->sockfd, &rset);
		}
again:
		/* Select is not affected by SA_RESTART flag in Solaris, why? no clue whatsoever. Works
		 * fine for Linux though */
		if (select(max_fd, &rset, NULL, NULL, NULL) < 0 ) {
			if (errno == EINTR)
				goto again;
			/* cleanup stuff before we exit, which really doesn't matter much since
			 * exit cleans up all the memory associated with the process, nevertheless lets do it*/
			else goto out;
		}

		/* handle client connections from here */
		for (i = 0; i < count; i++) {
			if (FD_ISSET(s[i]->sockfd, &rset)) {
				/* handle stuff here */
				/* most of the connection details are encapsulated within the common_ifi_info struct */
				socklen_t l = sizeof(struct sockaddr_in);
				Recvfrom(s[i]->sockfd, filename, sizeof(filename), 0, (void *)&s[i]->conn_addr , &l );
				/* at this point the server receives the filename, but it could happen so that the server sets up the connection socket
				 * and send the ephemeral port of the connection socket to the client and the client never gets it, so the client will timeout and 
				 * sends us the filename again, in that case we should just send the ephmereal port to the client */
				if (state == 0) {
					printf("<server> filename is %s\n", filename);
					pid = Fork();
					if (pid == 0) {
						/* child -- close the inherited sockfd, except the one in which the request came */
						int j;
						for (j = 0; j < count; j++) {
							if (j != i) 
								close(s[j]->sockfd);
						}
						int newfd, option = 1;
						struct sockaddr_in tmp;
						struct common_addr_info *c;
						int flag = 0;

						c = compute_longest_prefix(s, &s[i]->conn_addr.sin_addr , count);
						display_network_info(c);
						memset(&tmp, 0x0, sizeof(tmp));
						memcpy(&tmp, &s[i]->ip_addr, sizeof(struct sockaddr_in));
						tmp.sin_port = htons(0);
						newfd = Socket(AF_INET, SOCK_DGRAM , 0);
						/* if it belongs to the same subnet then set the DONTROUTE flag */
						if (c->route_flag == SAMESUBNET) {
							flag = MSG_DONTROUTE;
							Setsockopt(newfd, SOL_SOCKET, SO_DONTROUTE, &option, sizeof(option));
						}
						Bind(newfd, (void *)&tmp, sizeof(tmp));	
						int enable = 1;
						Setsockopt(newfd, SOL_SOCKET, SO_REUSEADDR, &enable , sizeof(int));

						Connect(newfd, (void *)&s[i]->conn_addr, sizeof(struct sockaddr_in));
						int e_port;
						e_port = display_sock_peername(newfd);

						/* now we need to send the ephemereal port no of the new connection to the server 
						 * via the sockfd */

						/* max port no is 65535 and space for a null byte */
						char port_buf[sizeof("65535") + 1];
						snprintf(port_buf, sizeof(port_buf),  "%d", e_port);
						printf("<server> new ephemereal port is %s\n", port_buf);
						Sendto(s[i]->sockfd, port_buf, sizeof(port_buf), 0, (void *) &s[i]->conn_addr, sizeof(struct sockaddr_in));

						char ack[4];
						Recvfrom(newfd, ack, sizeof(ack), 0, (void *) &s[i]->conn_addr, &l);
						printf("<server> Got ACK, closing the listening socket\n");	
						/* Once we have got the ACK, we can close the "listening" socket */
						close(s[i]->sockfd);

						/* do the rest of the stuff via the connection socket */
						handle_file_transfer( newfd, filename, flag, sinfo);
						printf("<server> Done with the client, waiting for the next one\n");
						exit(0);
					}
					/* at this point if subsequent file name comes, dont create child processes for them */
				}
				state = 0;
				/* parent goes back to the main loop */
			}
		}
	}
out:
	for (i = 0; i < count ; i++)
		if (s[i] != NULL)
			free(s[i]);
	if (s != NULL)
		free(s);
	return 0;
}
