#ifndef __CLIENT_H
#define __CLIENT_H
#include "unp.h" 
#include "unpthread.h"
#include "common_defines.h"
#include <limits.h>
#include <math.h>
/* this structure gets populated by reading from the client.in file */
struct cli_info {
	struct in_addr serv_addr;
	uint16_t port;
	char filename[PATH_MAX];
	int recv_swsz;
	int seed;
	float prob;
	int mu; 
};

struct cons_info {
	int sockfd;
	int mu;
};

struct scirc_buffer {
	void *data;
	int filled_entries;
	int elem_count;
	int prod_idx;
	int cons_idx;
   	uint8_t *valid;
};


struct scirc_buffer *init_scirc_buffer(int elem_sz, int elem_count)
{
	struct scirc_buffer *c = Malloc(sizeof(*c));
	/* lar, lds, filled entries set to 0 by memset */
	memset(c, 0x0, sizeof(*c));
	c->elem_count = elem_count;
	c->data = Malloc(elem_count * elem_sz);
	c->valid = Malloc(elem_count);
	memset(c->valid, 0x0, elem_count);
	memset(c->data, 0x0, elem_count * elem_sz);
	return c;
}

void add_to_scirc_buffer(struct scirc_buffer *c, struct dgram *data, int elem_sz, pthread_mutex_t m)
{
	/* no need to check for free space at this point since we did that before */

	/* last data sent controls adding to circ buffer */
	struct dgram *dg = (void *)c->data;
	int index = c->prod_idx; 
	memcpy(&dg[index], data, sizeof(*data));
	c->valid[c->prod_idx] = 1;
	c->prod_idx = ( c->prod_idx + 1 ) % c->elem_count;
	Pthread_mutex_lock(&m);
	c->filled_entries += 1;
	Pthread_mutex_unlock(&m);
}

void add_to_scirc_buffer_index(struct scirc_buffer *c, struct dgram *data, int elem_sz, int index, pthread_mutex_t m)
{
	struct dgram *dg = (void *)c->data;
	
	memcpy(&dg[index], data, sizeof(*data));
	c->valid[index] = 1;
	
	Pthread_mutex_lock(&m);
	c->filled_entries += 1;
	Pthread_mutex_unlock(&m);
	
}

struct dgram *remove_from_scirc_buffer(struct scirc_buffer *c, pthread_mutex_t m) {
	struct dgram *dg = (void *) c->data, *ret;


	int index = c->cons_idx;
	ret = &dg[index];
	c->valid[c->cons_idx] = 0;
	c->cons_idx = (c->cons_idx + 1) % c->elem_count;
	Pthread_mutex_lock(&m);
	c->filled_entries = (c->filled_entries - 1) ;
	Pthread_mutex_unlock(&m);
	return ret;

}

#endif
