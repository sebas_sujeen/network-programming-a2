#ifndef __SERVER_H
#define __SERVER_H
#include "unp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include "unprtt.h"
#include "unpifi.h"
#include "common_defines.h"

#define SLOW_START 0x1
#define CA 0x2
#define FRR 0x3

struct serv_info {
	int port;
	int ssw_sz;
	int rsw_sz;
};

struct scirc_buffer {
	void *data;
	int filled_entries;
	int elem_count;
	int lar;
	int lds;
   	uint8_t *valid;
};

/* stupid name since I have run out of names */
struct state_info {
	double cwnd;
	int flag;
	int ssthresh;
	int phase;
	struct prot_header p_hdr;
};


static int rttinit = 0;
static struct rtt_info rttinfo;

void init_prot_header(struct state_info *st, int flag)
{
	struct prot_header *p = &st->p_hdr;
	if (rttinit == 0) {
		rtt_init(&rttinfo);
		rttinit = 1;
		rtt_d_flag = 1;
	}
	memset(p, 0x0, sizeof(*p));
	p->seq_no = 0;
	p->ack_no = 0;
	p->timestamp = rtt_ts(&rttinfo);
	p->rsw_sz = INT_MAX;
	st->cwnd = (float)1;
	st->phase = SLOW_START;
	st->ssthresh = INT_MAX;
	st->flag = flag;
}

ssize_t
Read_mod(int fd, void *ptr, size_t nbytes)
{
	ssize_t		n;

again:
	if ( (n = read(fd, ptr, nbytes)) == -1) {
		if (errno == EINTR)
			goto again;
		err_sys("read error");
	}
	return(n);
}
struct scirc_buffer *init_scirc_buffer(int elem_sz, int elem_count)
{
	struct scirc_buffer *c = Malloc(sizeof(*c));
	/* lar, lds, filled entries set to 0 by memset */
	memset(c, 0x0, sizeof(*c));
	c->elem_count = elem_count;
	c->data = Malloc(elem_count * elem_sz);
	c->valid = Malloc(elem_count);
	memset(c->valid, 0x0, elem_count);
	memset(c->data, 0x0, elem_count * elem_sz);
	return c;
}

void add_to_scirc_buffer(struct scirc_buffer *c, struct dgram *data, int elem_sz)
{
	/* no need to check for free space at this point since we did that before */
	assert(c->filled_entries != c->elem_count);

	/* last data sent controls adding to circ buffer */
	struct dgram *dg = (void *)c->data;
	int index = c->lds; 
	assert(sizeof(*data) == DGRAM_SZ);
	memcpy(&dg[index], data, sizeof(*data));
	c->valid[c->lds] = 1;
	c->lds = ( c->lds + 1 ) % c->elem_count;
	c->filled_entries += 1;
}

struct dgram *remove_from_scirc_buffer(struct scirc_buffer *c) {
	struct dgram *dg = (void *) c->data, *ret;
	assert(c->filled_entries  != 0);

	int index = c->lar;
	ret = &dg[index];
	c->valid[c->lar] = 0;
	c->lar = (c->lar + 1) % c->elem_count;
	c->filled_entries = (c->filled_entries - 1) ;

	return ret;

}

void check_for_space(struct scirc_buffer *c, int elem_count)
{
	int space_avail = c->elem_count - c->filled_entries;
	/* need to take the valid entries into account as well */
	int i;
	for (i = c->lar; i < c->filled_entries; i++) {
		if (c->valid[i])
			space_avail ++;
		
	}
	if (space_avail < elem_count) {
		/* should expand, increase to c->elem_count * 2 + elem_count */
		int new_elem_count = c->elem_count * 2 + elem_count;
		c->data = realloc(c->data, new_elem_count * DGRAM_SZ);
		c->valid = realloc(c->valid, new_elem_count);

		uint8_t *ptr = c->valid + c->elem_count;
		memset(ptr, 0x0, new_elem_count - c->elem_count);
		c->elem_count = new_elem_count ;
	}
}

void construct_dgram(struct dgram *dg, void *data, size_t size, struct state_info *st)
{
	struct prot_header *p = &dg->p;
	struct prot_header *p_hdr = &st->p_hdr;
	memcpy(p, p_hdr, sizeof(*p));
	p_hdr->seq_no += 1;
	p_hdr->timestamp = rtt_ts(&rttinfo);
	/* cwnd and rwnd gets updated on reception of ACK */
	assert(size <= DGRAM_SZ - sizeof(*p));
	memcpy(dg->data, data, size);
	return ;
}

void display_dgram(struct dgram *dg)
{
	struct prot_header *p = (void *) dg;
	printf("seq no is %d\n", p->seq_no);
	char *data = (char *) dg + sizeof(*p);
	printf("data = %s\n", data);
}

void display_stats(struct scirc_buffer *c)
{
	printf("c->filled_entries = %d c->elem_count = %d c->lar = %d c->lds = %d\n", c->filled_entries, c->elem_count, c->lar, c->lds);
}


#endif
