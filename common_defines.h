#ifndef __COMMON_DEFINES_H
#define __COMMON_DEFINES_H
#include "unp.h"
#include "unpifi.h"
#include	<net/if.h>
#define	IFI_NAME	16			/* same as IFNAMSIZ in <net/if.h> */
#define	IFI_HADDR	 8			/* allow for 64-bit EUI-64 in future */
#define SAMEHOST 0x1
#define SAMESUBNET 0x2
#define DGRAM_SZ 512


struct circ_buffer {
	int elem_count;
	int elem_size; /* size of each element */
	int prod_index; /* the current producer index */
	int cons_index; /* the current consumer index */
	void *buf;
};

struct common_addr_info {
	int route_flag;
	struct in_addr src_addr;
	struct in_addr dst_addr;
};

struct common_ifi_info {
	int sockfd;
	int nw_length; /* no of bits representing the network portion of an IP address */
	struct sockaddr ip_addr; /* ip address */
	struct sockaddr snetmask;  /* subnet mask */
	struct sockaddr snetaddr;  /* subnet addr */
	struct sockaddr_in conn_addr;
};


struct ifi_info_mod {
  char    ifi_name[IFI_NAME];	/* interface name, null-terminated */
  short   ifi_index;			/* interface index */
  short   ifi_mtu;				/* interface MTU */
  u_char  ifi_haddr[IFI_HADDR];	/* hardware address */
  u_short ifi_hlen;				/* # bytes in hardware address: 0, 6, 8 */
  short   ifi_flags;			/* IFF_xxx constants from <net/if.h> */
  short   ifi_myflags;			/* our own IFI_xxx flags */
  struct sockaddr  *ifi_addr;	/* primary address */
  struct sockaddr  *ifi_brdaddr;/* broadcast address */
  struct sockaddr  *ifi_dstaddr;/* destination address */
  struct sockaddr  *ifi_ntmask; /* network mask */
  struct ifi_info_mod  *ifi_next;	/* next of these structures */
};

#define	IFI_ALIAS	1			/* ifi_addr is an alias */
struct prot_header {
	int seq_no;
	int ack_no;
	int timestamp;
	int rsw_sz;
	int eof_marker;
};

struct dgram {
	struct prot_header p;
	char data[DGRAM_SZ - sizeof(struct prot_header)];
};


struct ifi_info_mod *
get_ifi_info_mod(int family, int doaliases)
{
	struct ifi_info_mod		*ifi, *ifihead, **ifipnext;
	int					sockfd, len, lastlen, flags, myflags, idx = 0, hlen = 0;
	char				*ptr, *buf, lastname[IFNAMSIZ], *cptr, *haddr, *sdlname;
	struct ifconf		ifc;
	struct ifreq		*ifr, ifrcopy;
	struct sockaddr_in	*sinptr;
	struct sockaddr_in6	*sin6ptr;

	sockfd = Socket(AF_INET, SOCK_DGRAM, 0);

	lastlen = 0;
	len = 100 * sizeof(struct ifreq);	/* initial buffer size guess */
	for ( ; ; ) {
		buf = Malloc(len);
		ifc.ifc_len = len;
		ifc.ifc_buf = buf;
		if (ioctl(sockfd, SIOCGIFCONF, &ifc) < 0) {
			if (errno != EINVAL || lastlen != 0)
				err_sys("ioctl error");
		} else {
			if (ifc.ifc_len == lastlen)
				break;		/* success, len has not changed */
			lastlen = ifc.ifc_len;
		}
		len += 10 * sizeof(struct ifreq);	/* increment */
		free(buf);
	}
	ifihead = NULL;
	ifipnext = &ifihead;
	lastname[0] = 0;
	sdlname = NULL;
/* end get_ifi_info1 */

/* include get_ifi_info2 */
	for (ptr = buf; ptr < buf + ifc.ifc_len; ) {
		ifr = (struct ifreq *) ptr;

#ifdef	HAVE_SOCKADDR_SA_LEN
		len = max(sizeof(struct sockaddr), ifr->ifr_addr.sa_len);
#else
		switch (ifr->ifr_addr.sa_family) {
#ifdef	IPV6
		case AF_INET6:	
			len = sizeof(struct sockaddr_in6);
			break;
#endif
		case AF_INET:	
		default:	
			len = sizeof(struct sockaddr);
			break;
		}
#endif	/* HAVE_SOCKADDR_SA_LEN */
		ptr += sizeof(ifr->ifr_name) + len;	/* for next one in buffer */

#ifdef	HAVE_SOCKADDR_DL_STRUCT
		/* assumes that AF_LINK precedes AF_INET or AF_INET6 */
		if (ifr->ifr_addr.sa_family == AF_LINK) {
			struct sockaddr_dl *sdl = (struct sockaddr_dl *)&ifr->ifr_addr;
			sdlname = ifr->ifr_name;
			idx = sdl->sdl_index;
			haddr = sdl->sdl_data + sdl->sdl_nlen;
			hlen = sdl->sdl_alen;
		}
#endif

		if (ifr->ifr_addr.sa_family != family)
			continue;	/* ignore if not desired address family */

		myflags = 0;
		if ( (cptr = strchr(ifr->ifr_name, ':')) != NULL)
			*cptr = 0;		/* replace colon with null */
		if (strncmp(lastname, ifr->ifr_name, IFNAMSIZ) == 0) {
			if (doaliases == 0)
				continue;	/* already processed this interface */
			myflags = IFI_ALIAS;
		}
		memcpy(lastname, ifr->ifr_name, IFNAMSIZ);

		ifrcopy = *ifr;
		Ioctl(sockfd, SIOCGIFFLAGS, &ifrcopy);
		flags = ifrcopy.ifr_flags;
		if ((flags & IFF_UP) == 0)
			continue;	/* ignore if interface not up */
/* end get_ifi_info2 */

/* include get_ifi_info3 */
		ifi = Calloc(1, sizeof(struct ifi_info_mod));
		*ifipnext = ifi;			/* prev points to this new one */
		ifipnext = &ifi->ifi_next;	/* pointer to next one goes here */

		ifi->ifi_flags = flags;		/* IFF_xxx values */
		ifi->ifi_myflags = myflags;	/* IFI_xxx values */
#if defined(SIOCGIFMTU) && defined(HAVE_STRUCT_IFREQ_IFR_MTU)
		Ioctl(sockfd, SIOCGIFMTU, &ifrcopy);
		ifi->ifi_mtu = ifrcopy.ifr_mtu;
#else
		ifi->ifi_mtu = 0;
#endif
		memcpy(ifi->ifi_name, ifr->ifr_name, IFI_NAME);
		ifi->ifi_name[IFI_NAME-1] = '\0';
		/* If the sockaddr_dl is from a different interface, ignore it */
		if (sdlname == NULL || strcmp(sdlname, ifr->ifr_name) != 0)
			idx = hlen = 0;
		ifi->ifi_index = idx;
		ifi->ifi_hlen = hlen;
		if (ifi->ifi_hlen > IFI_HADDR)
			ifi->ifi_hlen = IFI_HADDR;
		if (hlen)
			memcpy(ifi->ifi_haddr, haddr, ifi->ifi_hlen);
/* end get_ifi_info3 */
/* include get_ifi_info4 */
		switch (ifr->ifr_addr.sa_family) {
		case AF_INET:
			sinptr = (struct sockaddr_in *) &ifr->ifr_addr;
			ifi->ifi_addr = Calloc(1, sizeof(struct sockaddr_in));
			memcpy(ifi->ifi_addr, sinptr, sizeof(struct sockaddr_in));

#ifdef	SIOCGIFBRDADDR
			if (flags & IFF_BROADCAST) {
				Ioctl(sockfd, SIOCGIFBRDADDR, &ifrcopy);
				sinptr = (struct sockaddr_in *) &ifrcopy.ifr_broadaddr;
				ifi->ifi_brdaddr = Calloc(1, sizeof(struct sockaddr_in));
				memcpy(ifi->ifi_brdaddr, sinptr, sizeof(struct sockaddr_in));
			}
#endif

#ifdef	SIOCGIFDSTADDR
			if (flags & IFF_POINTOPOINT) {
				Ioctl(sockfd, SIOCGIFDSTADDR, &ifrcopy);
				sinptr = (struct sockaddr_in *) &ifrcopy.ifr_dstaddr;
				ifi->ifi_dstaddr = Calloc(1, sizeof(struct sockaddr_in));
				memcpy(ifi->ifi_dstaddr, sinptr, sizeof(struct sockaddr_in));
			}
#endif
#ifdef SIOCGIFNETMASK
			Ioctl(sockfd, SIOCGIFNETMASK, &ifrcopy);
			sinptr = (struct sockaddr_in *) &ifrcopy.ifr_addr;
			ifi->ifi_ntmask = Calloc(1, sizeof(struct sockaddr_in));
			memcpy(ifi->ifi_ntmask, sinptr, sizeof(struct sockaddr_in));
#endif
			break;

		case AF_INET6:
			sin6ptr = (struct sockaddr_in6 *) &ifr->ifr_addr;
			ifi->ifi_addr = Calloc(1, sizeof(struct sockaddr_in6));
			memcpy(ifi->ifi_addr, sin6ptr, sizeof(struct sockaddr_in6));

#ifdef	SIOCGIFDSTADDR
			if (flags & IFF_POINTOPOINT) {
				Ioctl(sockfd, SIOCGIFDSTADDR, &ifrcopy);
				sin6ptr = (struct sockaddr_in6 *) &ifrcopy.ifr_dstaddr;
				ifi->ifi_dstaddr = Calloc(1, sizeof(struct sockaddr_in6));
				memcpy(ifi->ifi_dstaddr, sin6ptr, sizeof(struct sockaddr_in6));
			}
#endif
			break;

		default:
			break;
		}
	}
	free(buf);
	return(ifihead);	/* pointer to first structure in linked list */
}
/* end get_ifi_info4 */

/* include free_ifi_info */
void
free_ifi_info_mod(struct ifi_info_mod *ifihead)
{
	struct ifi_info_mod	*ifi, *ifinext;

	for (ifi = ifihead; ifi != NULL; ifi = ifinext) {
		if (ifi->ifi_addr != NULL)
			free(ifi->ifi_addr);
		if (ifi->ifi_brdaddr != NULL)
			free(ifi->ifi_brdaddr);
		if (ifi->ifi_dstaddr != NULL)
			free(ifi->ifi_dstaddr);
		if (ifi->ifi_ntmask != NULL)
			free(ifi->ifi_ntmask);
		
		ifinext = ifi->ifi_next;	/* can't fetch ifi_next after free() */
		free(ifi);					/* the ifi_info{} itself */
	}
}
/* end free_ifi_info */

struct ifi_info_mod *
Get_ifi_info_mod(int family, int doaliases)
{
	struct ifi_info_mod	*ifi;

	if ( (ifi = get_ifi_info_mod(family, doaliases)) == NULL)
		err_quit("get_ifi_info error");
	return(ifi);
}
/* displays the sockname and peername and also returns the port no of the sock */
int display_sock_peername(int sockfd)
{
	struct sockaddr_in tmp;
	int e_port;
	socklen_t len = sizeof(tmp);
	memset(&tmp, 0x0, sizeof(tmp));
	Getsockname(sockfd, (void *)&tmp, &len);
	printf("Sock name is %s and port is %d\n", Sock_ntop_host((void *)&tmp, len), ntohs(tmp.sin_port));
	e_port = ntohs(tmp.sin_port);
	
	len = sizeof(tmp);
	memset(&tmp, 0x0, sizeof(tmp));
	Getpeername(sockfd, (void *)&tmp, &len);
	printf("Peer name is %s and port is %d\n", Sock_ntop_host((void *)&tmp, len), ntohs(tmp.sin_port));
	return e_port;
}

char *populate_prot_data(FILE *fp, int hdr_bytes)
{
	int fd = fileno(fp);
	int n;
	char *p = Malloc(DGRAM_SZ - hdr_bytes);
	memset(p, 0x0, DGRAM_SZ - hdr_bytes);
	/* leave space for a NULL byte */
	if ( (n = Read(fd, p, DGRAM_SZ - hdr_bytes - 1)) == 0) {
		/* free here to avoid mem leaks */
		free(p);
		return NULL;
	}
	return p;
}

/* seq_no is value-result argument */
struct prot_header * populate_prot_header(int *seq_no, struct prot_header *p)
{
	p->seq_no = *seq_no;
	*seq_no += 1;
	return p;
}

/* */
struct common_addr_info *compute_longest_prefix(struct common_ifi_info **s, struct in_addr *a2, int count)
{

	int i;
	struct in_addr *a1;
	int *p, *q;
	int shift;
	int v1, v2;
	struct common_addr_info *c;
	struct sockaddr_in *s1;

	c = Malloc(sizeof(*c));
	q = (int *) a2;
	/* check if they belong to the same host */
	for (i = 0; i < count; i++) {
		s1 = (void *)&s[i]->ip_addr;
		a1 =  &s1->sin_addr;
		p = (int *) a1;
		if ( *p == *q) {
		    /*  If they belong to the same host, then both the src addr is 127.0.0.1 */	
			c->route_flag = SAMEHOST;
			Inet_pton(AF_INET, "127.0.0.1", &c->src_addr);
			memcpy(&c->dst_addr, &c->src_addr, sizeof(c->dst_addr));
			return c;
		}
	}
	/* convert to little endian format */
	int nw_addr = ntohl(*q) ;
	for (i = 0; i < count; i++) {
		shift = 32 - s[i]->nw_length;
		s1 = (void *)&s[i]->ip_addr;
		a1 =  &s1->sin_addr;
		p = (int *)a1;
		v1 = (ntohl(*p) >> shift) << shift;
		v2 = (nw_addr >> shift) << shift;
		if (v1 == v2) {
			c->route_flag = SAMESUBNET;
			memcpy(&c->src_addr, a1, sizeof(c->src_addr));
			memcpy(&c->dst_addr, a2, sizeof(c->dst_addr));
			return c;
		}
	}
	c->route_flag = 0x10;
	/* This could be anything, but lets take the last one..assume we have atleast one interface that is UP
	 * or else it is an underflow.. */
	s1 = (void *)&s[i - 1]->ip_addr;
	memcpy(&c->src_addr, &s1->sin_addr, sizeof(c->src_addr));
	memcpy(&c->dst_addr, a2, sizeof(c->dst_addr));
	return c;	
}

static void sort_by_nwlen(struct common_ifi_info **s, int count)
{
	/* no of interfaces too small to try fancy sorting algorithms */
	int i, j;

	for (i = 0; i < count; i++) {
		for (j = i + 1; j < count; j++) {
			if (s[i]->nw_length < s[j]->nw_length) {
				struct common_ifi_info *tmp = s[i];
				s[i] = s[j];
				s[j] = tmp;
			}
		}
	}

}

static int get_nw_length(struct sockaddr_in *s)
{
	unsigned int *p = (void *)&s->sin_addr.s_addr;
	return (32 - __builtin_ctz(ntohl(*p))); 
}

struct common_ifi_info **populate_ifi_info(int count)
{

	struct ifi_info_mod *ifi, *ifihead;
	struct common_ifi_info **s;
	struct sockaddr *sa;
	int i;

	/* Allocate space for the pointers and then the actual struct in a loop.. */
	s = Malloc(sizeof(*s) * count);
	for (i = 0; i < count ; i++) {
		s[i] = Malloc(sizeof(**s));
	}	
	/* get individual interface address and subnet mask */
	i = 0;

	/* this loop populates the array of common_ifi_info, in particular, it populates the 
	 * ip addr, subnet mask, subnet addr and the sockfd. We need to bind each of the sockfd to
	 * a well known port taken from server.in */
	for (ifihead = ifi = Get_ifi_info_mod(AF_INET, 0);
			ifi != NULL && i < count; ifi = ifi->ifi_next, i++) {

		if ( (sa = ifi->ifi_addr) != NULL) {
			memcpy(&s[i]->ip_addr, sa, sizeof(*sa));
			printf("IP addr: %s\n",
					Sock_ntop_host(&s[i]->ip_addr, sizeof(*sa)));
		}
		if ( (sa = ifi->ifi_ntmask) != NULL) {
			memcpy(&s[i]->snetmask, sa, sizeof(*sa));
			printf("net mask: %s\n",
					Sock_ntop_host(&s[i]->snetmask, sizeof(*sa)));
		}

		struct sockaddr_in subnet_addr;
		struct sockaddr_in *s1, *s2;
		s1 = (void *) ifi->ifi_addr;
		s2 = (void *) ifi->ifi_ntmask;

		/* subnet address is the bitwise and of subnet mask and the ip addr, this could be 
		 * written in a more cleaner way, but Sock_ntop_host requires a sockaddr, so stuck with this */
		memset(&subnet_addr, 0x0, sizeof(subnet_addr));
		subnet_addr.sin_family = AF_INET;
		subnet_addr.sin_addr.s_addr = s1->sin_addr.s_addr & s2->sin_addr.s_addr;
		printf("Subnet addr : %s\n", Sock_ntop_host((void *)&subnet_addr, sizeof(subnet_addr)));
		s[i]->nw_length = get_nw_length(s2);	
	}
	/* it would make sense to sort the common_ifi_info array in decreasing order of the 
	 * network address length, so that longest prefix match can be implemented easily */
	sort_by_nwlen(s, count);
	free_ifi_info_mod(ifihead);
	return s;
}

static int get_ifi_count(void)
{
	int count = 0;
	struct ifi_info_mod *ifi;

	/* get the count of interfaces we care about, so that we can avoid statically allocating common_ifi_info */
	ifi = Get_ifi_info_mod(AF_INET, 0);
	while (ifi != NULL) {
		count ++;
		ifi = ifi->ifi_next;
	}

	return count;
}


void display_network_info(struct common_addr_info *c) {

	char buf[INET_ADDRSTRLEN];
	char buf1[INET_ADDRSTRLEN];
	Inet_ntop(AF_INET, &c->dst_addr, buf, sizeof(buf));
	Inet_ntop(AF_INET, &c->src_addr, buf1,  sizeof(buf));
	switch(c->route_flag) {
		case SAMEHOST:
			printf("<server> %s and %s belongs to the same host\n", buf1, buf);
			break;
		case SAMESUBNET:
			printf("<server> %s and %s belongs to the same subnet\n", buf1, buf);
			break;
		default:
			printf("<server> %s and %s belongs to different subnet\n", buf1, buf);

	}
	printf("<server> IPClient: %s, IPServer: %s\n", buf1, buf);
}

/* API for circular ring buffer, to be used for sender's sliding window, receiver window and for synchronizing between
 * the client threads to print file contents onto stdout */

struct circ_buffer *init_circ_buffer(int elem_count, int elem_size)
{
	struct circ_buffer *c = Malloc(sizeof(*c));
	c->elem_count = elem_count;
	c->elem_size = elem_size;
	/* Avoid integer overflows */
	if (elem_count * elem_size < elem_size ) {
		fprintf(stderr, "integer overflow detected\n");
		exit(-1);
	}
	c->buf = Malloc(elem_count * elem_size);
	memset(c->buf, 0x0, elem_count * elem_size);
	c->prod_index = 0;
	c->cons_index = 0;
	return c;
}

static inline void *get_index_addr(struct circ_buffer *c, int index)
{
	int elem_size = c->elem_size;
	char *p = (char *) c->buf;
	p = p + (index * elem_size);
	return p;
}

void add_to_circ_buffer(struct circ_buffer *c, void *data, size_t elem_size)
{
	/* do some sanity checks first */
	if (c->elem_size < elem_size) {
		fprintf(stderr, "can't add %d sized element to circ buffer\n", elem_size);
		exit(-1);
	}
	/* it just adds the element to the circ buffer, other sanity checks are not needed, since this code
	 * will be protected by condition variables. */

	/* note that the producer usually calls add_to_circ_buffer, which touches the prod_index, while the consumer
	 * touches the cons_index, there is no need of synchronization between them except in the case when the circ_buffer
	 * is full or when it is empty, those two cases are handled by condition variables in the client code */
	void *index_addr = get_index_addr(c, c->prod_index);
	memcpy(index_addr, data, elem_size);
	c->prod_index = (c->prod_index + 1) % c->elem_count;

}

void remove_from_circ_buffer(struct circ_buffer *c, void *data, size_t elem_size)
{
	/* the second and the third args are for performing the sanity checks here */
	if (elem_size < c->elem_size) {
		fprintf(stderr, "can't store %d sized element from circ buffer to user buf\n", c->elem_size);
		exit(-1);	
	}
	void *index_addr = get_index_addr(c, c->cons_index);
	memcpy(data, index_addr, c->elem_size);
	c->cons_index = (c->cons_index + 1) % c->elem_count;
}

float get_rand()
{
	return (float)rand() / (float)RAND_MAX ;
}

static int calc(float p)
{
	float d = get_rand();

	if (d < p)
		return 1;
	else
		return 0;
}

void Write_with_prob(int sockfd, void *data, int size, float prob)
{
	int shld_write = calc(prob);
	struct prot_header *p = data;
	
	if (shld_write) {
		printf("<client> Sending dgram : ack_no = %d window size = %d eof_marker = %d\n", p->ack_no, p->rsw_sz, p->eof_marker);
		Write(sockfd, data, size);
	} else {
		printf("<client> Dropping dgram : ack_no = %d window size = %d eof_marker = %d\n", p->ack_no, p->rsw_sz, p->eof_marker);
	}
}


void del_circ_buffer(struct circ_buffer *c)
{
	free(c->buf);
	free(c);
}

#endif
